///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Liam Tapper <tliam@hawaii.edu>
/// @date    24/01/2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>
using namespace std;
// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
   //std::cout << argc << std::endl;
   //std::cout << argc << std::endl;
   if (argv[1] != NULL){
      for (int i = argc; i > 0; i--){
	      std::cout << argv[i -1] << std::endl;
      }
   }else {
      int counter = 0;
      while(envp[counter] != NULL){
         std::cout << envp[counter] << std::endl;
         counter++;
      }
   }


   std::cout << "DONE" << std::endl;
   std::exit( EXIT_SUCCESS );

}
